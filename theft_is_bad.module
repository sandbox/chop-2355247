<?php

/**
 * @file
 * Theft is bad module.
 *
 * This module will prevent theft of content from Australian Surf Travel
 * web-sites powered by Drupal. It is also provides customisation of warnings
 * so that it might work on non-Surf Travel web-sites too.
 *
 * The original idea and client-side code for this module came from an
 * excellent content theft prevention implementation on
 * http://www.surftravel.com.au/
 *
 * All thanks go to the unknown developers of that ground-breaking innovation.
 */

const THEFT_IS_BAD_DEFAULT_MESSAGES_TEXT = 'Don\'t go there!' . PHP_EOL . 'Theft is a crime.' . PHP_EOL . 'You have been warned.' . PHP_EOL . 'Seriously?' . PHP_EOL . 'Theft is bad!\nBad is coming to get you!' . PHP_EOL . 'Your details have been recorded.\nStart running!';
const THEFT_IS_BAD_DEFAULT_STATUS = 0;
const THEFT_IS_BAD_STATUS_DISABLE = 0;
const THEFT_IS_BAD_STATUS_ENABLE = 1;
const THEFT_IS_BAD_STATUS_CUSTOM = 2;

/**
 *  Implements hook_menu().
 */
function theft_is_bad_menu() {
  $items = array();

  $items['admin/config/system/theft-is-bad'] = array(
    'title' => 'Theft is bad settings',
    'description' => 'Administer Theft is bad settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('theft_is_bad_admin_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'theft_is_bad.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_help().
 */
function theft_is_bad_help($path) {
  $help = '';
  $replacements = array();
  $replacements['!theft_is_bad_admin'] = url(
    'admin/config/services/theft-is-bad'
  );
  $replacements['!theft_is_bad_admin'] = url(
    'admin/config/services/theft-is-bad'
  );
  $replacements['!theft_is_bad_example_site'] =
    'http://www.surftravel.com.au/';

  if ($path === 'admin/help#theft_is_bad') {
    // Main module help for the Theft is bad module.
    $help[] = <<<__END_OF_THEFT_IS_BAD_MODULE_HELP_1__
Theft is bad! for Drupal provides a simple yet effective method to prevent bad,
unwanted and illegal theft of copyrighted content from your
<a href="!drupal_powered_site">Drupal powered</a>
<a href="!theft_is_bad_example_site">Australian Surf Travel</a> web-site.
__END_OF_THEFT_IS_BAD_MODULE_HELP_1__;

    $help[] = <<<__END_OF_THEFT_IS_BAD_MODULE_HELP_2__
To configure and enable Australian Surf Travel content protection go to the
<a href="!theft_is_bad_admin">Theft is bad administration page</a>.
__END_OF_THEFT_IS_BAD_MODULE_HELP_2__;

  }
  elseif ($path === 'admin/config/services/theft-is-bad') {
    // Qubit administration settings help.
    $help[] = <<<__END_OF_THEFT_IS_BAD_ADMIN_HELP_1__
This page allows you to configure the Theft is bad content protection for your
<a href="!drupal_powered_site">Drupal powered</a>
<a href="!theft_is_bad_example_site">Australian Surf Travel</a> web-site.
If you are unsure what these settings mean, then leave them set to their
default values.
__END_OF_THEFT_IS_BAD_ADMIN_HELP_1__;

    $help[] = <<<__END_OF_THEFT_IS_BAD_ADMIN_HELP_2__
If you are unsure what these settings mean, then leave them set to their
default values. The default values have proven to work well with most
<a href="!theft_is_bad_example_site">Australian Surf Travel</a> web-sites.
__END_OF_THEFT_IS_BAD_ADMIN_HELP_2__;

  }

  $help_html = NULL;
  if (is_array($help)) {
    foreach ($help as $paragraph) {
      $help_html .= '<p>' . t($paragraph, $replacements) . '</p>';
    }
  }
  elseif (!empty($help)) {
    $help_html = '<p>' . t($help, $replacements) . '</p>';
  }

  return $help_html;
}

/**
 * Implements hook_preprocess_page().
 */
function theft_is_bad_preprocess_page(&$variables) {
  if (!is_array($variables)
      || empty($variables)
      || path_is_admin(current_path())
  ) {

    return;
  }

  $status = variable_get(
    'theft_is_bad_status', THEFT_IS_BAD_DEFAULT_STATUS
  );

  if ($status != THEFT_IS_BAD_STATUS_DISABLE) {
    // include original theft is bad javascript library
    $path_to_module = drupal_get_path('module', 'theft_is_bad');
    $path_to_script = $path_to_module . '/scripts/functions.js';
    $script_options = array(
      'type' => 'file',
      'group' => JS_LIBRARY,
    );
    drupal_add_js($path_to_script, $script_options);

    $script_settings = array();
    $script_settings['theft_is_bad']['status'] = $status;

    if ($status == THEFT_IS_BAD_STATUS_ENABLE) {
      $messages = THEFT_IS_BAD_DEFAULT_MESSAGES_TEXT;
    }
    elseif ($status == THEFT_IS_BAD_STATUS_CUSTOM) {
      $messages = variable_get(
        'theft_is_bad_messages_text', THEFT_IS_BAD_DEFAULT_STATUS
      );
    }
    $message_array = explode(PHP_EOL, $messages);
    $script_settings['theft_is_bad']['messages'] = $message_array;
    drupal_add_js($script_settings, 'setting');
  }

}
