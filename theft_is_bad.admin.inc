<?php

/**
 * @file
 * Theft is bad administration settings forms and pages.
 */


/**
 * Theft is bad administration Form API definition.
 *
 * @return array
 *  Drupal form API array
 */
function theft_is_bad_admin_form() {
  $form = array();

  $form['status']['theft_is_bad_status'] = array(
    '#type' => 'radios',
    '#title' => t('Theft detection warnings'),
    '#description' => t(
      'Display warning messages when bad theft is detected on Australian Surf Travel content pages.'
    ),
    '#default_value' => variable_get(
      'theft_is_bad_status', THEFT_IS_BAD_DEFAULT_STATUS
    ),
    '#options' => array(
      THEFT_IS_BAD_STATUS_DISABLE => t('Disable'),
      THEFT_IS_BAD_STATUS_ENABLE => t('Standard'),
      THEFT_IS_BAD_STATUS_CUSTOM => t('Custom'),
    ),
    '#required' => TRUE,
  );

  $form['custom'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom settings'),
    '#description' => t(
      'Use a customised Theft is bad, with your own warnings and settings.'
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="theft_is_bad_status"]' => array(
          'value' => THEFT_IS_BAD_STATUS_CUSTOM,
        ),
      ),
    ),
  );

  $form['custom']['theft_is_bad_messages_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Warning messages'),
    '#description' => t(
      'Enter warning messages text, with one message per line. ' .
      'Warnings should be successively stronger on each new line, ' .
      'with the strongest warning being on the last line.'
    ),
    '#default_value' => variable_get(
      'theft_is_bad_messages_text', THEFT_IS_BAD_DEFAULT_MESSAGES_TEXT
    ),
    '#cols' => 60,
    '#rows' => 5,
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
