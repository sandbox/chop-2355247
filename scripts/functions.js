/**
 * @file
 * Theft is bad!
 * 
 * Right-click handler for Surf Travel websites. Code taken unedited from 
 * http://www.surftravel.com.au/ on 10 Sep 2014.
 * 
 * @NickS 
 *   Right click a couple of times on this website http://www.surftravel.com.au/
 *   
 *   And then beware of the cyber police
 *   
 * @Vova
 *   Lol @NickS... I need that extension for Drupal!
 */

(function ($) {

    var SC = 0;
    var scmsg = ["Don't go there!", "Theft is a crime.", "You have been warned.", "Seriously?", "Theft is bad!\nBad is coming to get you!", "Your details have been recorded.\nStart running!"];
    //var scmsg = Drupal.settings.theft_is_bad.messages;
    function SA() {
        switch (SC) {
            case 0:
                SC = 1;
                break;
            case 1:
                SC = 2;
                break;
            case 2:
                SC = 3;
                break;
            case 3:
                SC = 4;
                break;
            case 4:
                SC = 5;
                break;
            case 5:
                SC = 6;
                break;
            default:
                break;
        }
        if (SC > 1) {
            alert(scmsg[SC - 1]);
        }
        ;
        return false;
    }

    var AN = navigator.appName;
    var NS = (AN == "Netscape") ? 1 : 0;
    if (AN == "Netscape") {
        document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);
    }
    function misc() {
        SA();
        return false;
    }

    function MH(e) {
        var myevent = (NS) ? e : event;
        var eventbutton = (NS) ? myevent.which : myevent.button;
        if ((eventbutton == 2) || (eventbutton == 3)) {
            e.preventDefault();
            return false;
        }
    }

    var LI = false;
    if (location.href.toLowerCase().indexOf("_admin") > 0) {
        LI = true;
    }
    if ($.cookie('LI') != null) {
        LI = true;
    }
    if (!LI) {
        document.oncontextmenu = misc;
        document.onmousedown = MH;
        document.onmouseup = MH;
        document.ondragstart = MH;
    }

})(jQuery);